# CMS Tweaks

Opinionated improvements to work with the Silverstripe cms

## Installation

```
composer require bhofstaetter/cms-tweaks
```

optional but recommended

```
composer require bhofstaetter/framework-tweaks
```

## Configuration

See ``config.yml`` and refer to the included models readme file

## Contents

- Fontawesome 6 icons for menu and SiteTree. To use FA Pro switch out ``SilverStripe\Admin\LeftAndMain.custom_requirements_css.fontawesome``
- ``allowed_parents`` and ``updateAllowedParents(&$parents)`` similar to ``allowed_children``
- ``$CurrentLevel`` current level in Sitetree
- ``$ShowTitle`` show page title or not
- ``$ShowSubNavigation`` show sub navigation or not
- ``$ShowBreadcrumbs`` show sub breadcrumbs or not
- Option to automatically update URLSegment field
- Some german translations
- Improvements for redirected urls
- A HomePage
- + more features from included modules

## Included Modules

- [Better Navigator](https://github.com/jonom/silverstripe-betternavigator)
- [Meta Title](https://github.com/kinglozzer/silverstripe-metatitle)
- [Google Sitemaps](https://github.com/wilr/silverstripe-googlesitemaps)
- [Shared Draft Content](https://github.com/silverstripe/silverstripe-sharedraftcontent)
- [Redirected Urls](https://github.com/silverstripe/silverstripe-redirectedurls)
- [Robots](https://github.com/tractorcow/silverstripe-robots)
