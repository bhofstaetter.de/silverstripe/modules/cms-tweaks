ss.i18n.addDictionary('de', {
  'ShareDraftContent.LOADING' : "Lädt...",
  'ShareDraftContent.FETCH_ERROR' : "Es gab ein Problem beim erstellen des Teilen-Links",
  'ShareDraftContent.DESCRIPTION' : "Jeder mit diesem Link kann den Entwurfsinhalt der Seite betrachten",
  'ShareDraftContent.LEARN_MORE' : "Mehr erfahren",
  'ShareDraftContent.LINK_HELP' : "Link zum Teilen des Entwurfinhalts",
  'ShareDraftContent.SHARE' : "Teilen",
  'ShareDraftContent.SHARE_DRAFT_CONTENT' : "Teile den Entwurfsinhalt"
});
