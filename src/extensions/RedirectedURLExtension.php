<?php

namespace bhofstaetter\CMSTweaks;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\ValidationResult;

class RedirectedURLExtension extends DataExtension {

    private static $db = [
        'Name' => 'Varchar(512)',
    ];

    private static array $required_fields = [
        'FromBase',
    ];

    private static array $duplicate_check_conditions = [
        ['FromBase', 'FromQuerystring'],
    ];

    public function onBeforeWrite() {
        if ($this->owner->RedirectionType === 'Internal') {
            $this->owner->To = null;
        } else if ($this->owner->RedirectionType === 'External') {
            $this->owner->LinkToID = null;
        }

        $this->owner->Name = $this->createName();
    }

    public function onBeforeDuplicate($original, $doWrite, $relations): void {
        $this->owner->FromBase = $this->owner->FromBase . '_' . _t('bhofstaetter\\FrameworkTweaks.Copy', 'Copy') . '_' . time();
        $this->owner->FromQuerystring = $this->owner->FromQuerystring . '_' . _t('bhofstaetter\\FrameworkTweaks.Copy', 'Copy') . '_' . time();
    }

    public function validate(ValidationResult $result) {
        $fromBase = $this->owner->FromBase;

        if (
            str_contains($fromBase, 'http://')
            || str_contains($fromBase, 'https://')
            || str_starts_with($fromBase, '//')
        ) {
            $result->addFieldError('FromBase', _t(__CLASS__ . '.FromBaseContainsHttpError', 'Url base could not contain http(s):// or a domain name'));
        }

        if (
            $this->owner->RedirectionType === 'External'
            && !str_starts_with($this->owner->To, '/')
            && !str_starts_with($this->owner->To, 'http://')
            && !str_starts_with($this->owner->To, 'https://')
        ) {
            $result->addFieldError('To', _t(__CLASS__ . '.ToHasWrongTargetError', 'Target needs to be a relative path or external url starting with http(s)://'));
        }

        if (
            $this->owner->RedirectionType === 'Internal'
            && !$this->owner->LinkTo()->exists()
        ) {
            $result->addFieldError('LinkToID', _t(__CLASS__ . '.LinkToDoesNotExistError', 'Please choose an existing page'));
        }
    }

    private function createName() {
        $queryString = $this->owner->FromQuerystring
            ? '?' . ltrim($this->owner->FromQuerystring, '?')
            : null;

        return $this->owner->FromBase . $queryString . ' ➝ ' . $this->Target();
    }

    public function updateSummaryFields(&$fields) {

        $fields = [
            'FromBase' => _t($this->owner->ClassName . '.FIELD_TITLE_FROMBASE', 'From URL base'),
            'FromQuerystring' => _t($this->owner->ClassName . '.FIELD_TITLE_FROMQUERYSTRING', 'From URL query parameters'),
            'Target' => _t(__CLASS__ . '.Target', 'Target'),
            'RedirectionType' => _t($this->owner->ClassName . '.FIELD_TITLE_REDIRECTIONTYPE', 'Redirection type'),
            'RedirectCode' => _t($this->owner->ClassName . '.FIELD_TITLE_REDIRECTCODE', 'Redirect code'),
        ];
    }

    public function updateCMSFields(FieldList $fields) {
        $fields->removeByName('Name');
    }

    public function Target() {
        if ($this->owner->RedirectionType === 'External') {
            return $this->owner->To;
        } else if ($this->owner->RedirectionType === 'Internal') {
            if ($this->owner->LinkTo()->exists()) {
                return $this->owner->LinkTo()->Title;
            }

            return _t(__CLASS__ . '.LinkToTargetError', 'Target seems to be deleted');
        }
    }
}
