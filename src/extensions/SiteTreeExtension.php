<?php

namespace bhofstaetter\CMSTweaks;

use SilverStripe\Core\ClassInfo;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HiddenField;
use SilverStripe\ORM\DataExtension;

class SiteTreeExtension extends DataExtension {

    /**
     * Indicates what kind of parents this page type can have.
     * This can be an array of allowed parent classes, or the string "none" -
     * indicating that this page type can't be a child page.
     * In this case {@link $can_be_root} must be true.
     *
     * If a classname is prefixed by "*", such as "*Page", then only that
     * class is allowed - no subclasses. Otherwise, the class and all its
     * subclasses are allowed.
     *
     * Note that this setting is cached when used in the CMS, use the "flush" query parameter to clear it.
     */
    private static array|string $allowed_parents = [];
    protected static array $_allowedChildrenForParent = [];

    private static array $db = [
        'CurrentLevel' => 'Int',
        'ShowTitle' => 'Boolean(1)',
        'ShowBreadcrumbs' => 'Boolean(1)',
        'ShowSubNavigation' => 'Boolean(1)',
        'AutoUpdateURLSegment' => 'Boolean(1)',
        'RobotsMetaTag' => "Enum('index follow, index nofollow, noindex follow, noindex nofollow','index follow')",
    ];

    private static array $required_fields = [
        'Title',
        'getMenuTitle',
    ];

    private static array $summary_fields = [
        'Title',
        'MenuTitle',
        'ClassName'
    ];

    public function onBeforeWrite(): void {
        $this->saveLevel();
        $this->saveURLSegment();
        $this->enforceValues();
    }
    private function saveLevel(): void {
        $owner = $this->owner;

        if (
            !$owner->Level
            || $owner->isChanged('ParentID', 2)
        ) {
            $page = $owner;
            $level = 1;

            while ($page->Parent()->exists()) {
                $page = $page->Parent();
                $level++;
            }

            $owner->CurrentLevel = $level;
        }
    }

    private function saveURLSegment(): void {
        $owner = $this->owner;

        if (
            $owner->AutoUpdateURLSegment
            && $owner->isChanged('Title',2)
            && !$owner->isChanged('URLSegment',2)
        ) {
            $this->owner->URLSegment = $this->owner->generateURLSegment($this->owner->Title);
        }
    }

    private function enforceValues(): void {
        // Unset ShowBreadcrumbs for top level pages
        if (
            !$this->owner->ParentID
            && $this->owner->ShowBreadcrumbs
        ) {
            $this->owner->ShowBreadcrumbs = false;
        }
    }

    public function updateCMSFields(FieldList $fields): void {
        $fields->push(HiddenField::create('CurrentLevel'));

        $fields->insertBefore(
            'MetaDescription',
            DropdownField::create('RobotsMetaTag', 'Robots-Tag', [
                'index follow' => _t(__CLASS__ . '.IndexFollow', 'index follow'),
                'index nofollow' => _t(__CLASS__ . '.IndexNoFollow', 'index nofollow'),
                'noindex follow' => _t(__CLASS__ . '.NoIndexFollow', 'noindex follow'),
                'noindex nofollow' => _t(__CLASS__ . '.NoIndexNoFollow', 'noindex nofollow'),
            ], 'index follow')
                ->setRightTitle(_t(__CLASS__ . '.RobotsMetaTagDesc', 'How to interact with search engines'))
                ->addExtraClass('help')
        );
    }

    public function updateSettingsFields(FieldList $fields): void {
        $fields->insertAfter(
            'ShowInSearch',
            CheckboxField::create('ShowSubNavigation', _t(__CLASS__ . '.ShowSubNavigation', 'Show sub navigation?'))
        );

        $fields->insertAfter(
            'ShowSubNavigation',
            CheckboxField::create('ShowTitle', _t(__CLASS__ . '.ShowTitle', 'Show page title?'))
        );

        if ($this->owner->ParentID) {
            $fields->insertAfter(
                'ShowTitle',
                CheckboxField::create('ShowBreadcrumbs', _t(__CLASS__ . '.ShowBreadcrumbs', 'Show breadcrumbs?'))
            );
        }

        $fields->insertBefore(
            'Visibility',
            DropdownField::create(
                'AutoUpdateURLSegment',
                _t(__CLASS__ . '.AutoUpdateURLSegment', 'Auto update url-segment'),
                [
                    true => _t(__CLASS__ . '.AutoUpdateURLSegmentTrue', 'yes'),
                    false => _t(__CLASS__ . '.AutoUpdateURLSegmentFalse', 'no')
                ]
            )
        );
    }

    public function MetaComponents(array &$tags): void {
        $tags['robots'] = [
            'attributes' => [
                'name' => 'robots',
                'content' => str_replace(' ', ', ', $this->owner->RobotsMetaTag ? $this->owner->RobotsMetaTag : ''),
            ],
        ];
    }

    public function allowedParents(): array|string {
        $parents = $this->owner->config()->get('allowed_parents');

        if ($this->owner->hasMethod('updateAllowedParents')) {
            $this->owner->updateAllowedParents($parents);
        }

        $this->owner->extend('updateAllowedParents', $parents);

        return $parents;
    }

    public function updateAllowedChildren(&$children): void {
        $parentClass = $this->owner->ClassName;

        if (isset(static::$_allowedChildrenForParent[$parentClass])) {
            $children = static::$_allowedChildrenForParent[$parentClass];
        } else {
            foreach ($children as $key => $childClassName) {
                if ($allowedParents = $childClassName::singleton()->allowedParents()) {
                    if ($allowedParents === 'none') {
                        unset($children[$key]);
                        continue;
                    }

                    $allowedParentsIncludingSubClasses = [];

                    foreach ($allowedParents as $allowedParentClass) {
                        if (str_starts_with($allowedParentClass, '*')) {
                            $allowedParentsIncludingSubClasses[] = ltrim($allowedParentClass, '*');
                        } else {
                            $allowedParentsIncludingSubClasses = array_merge(
                                $allowedParentsIncludingSubClasses,
                                ClassInfo::subclassesFor($allowedParentClass)
                            );
                        }
                    }

                    if (!in_array($parentClass, $allowedParentsIncludingSubClasses)) {
                        unset($children[$key]);
                    }
                }
            }

            static::$_allowedChildrenForParent[$parentClass] = $children;
        }
    }
}
