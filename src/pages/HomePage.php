<?php

namespace bhofstaetter\CMSTweaks;

use SilverStripe\CMS\Controllers\RootURLController;
use SilverStripe\Control\Director;

class HomePage extends \Page {

    private static $max_items_count = 1;
    private static $force_on_after_write = true;
    private static $can_be_root = true;
    private static $allowed_parents = 'none';
    private static $table_name = 'HomePage';

    private static $defaults = [
        'ShowTitle' => false,
        'ShowBreadcrumbs' => false,
        'AutoUpdateURLSegment' => false,
    ];

    public function populateDefaults() {
        parent::populateDefaults();
        $this->URLSegment = self::homepage_url_segment();
    }

    public function onBeforeWrite() {
        parent::onBeforeWrite();
        $this->enforceValues();
    }

    private function enforceValues(): void {
        if ($this->URLSegment != self::homepage_url_segment()) {
            $this->URLSegment = self::homepage_url_segment();
        }

        if ($this->AutoUpdateURLSegment) {
            $this->AutoUpdateURLSegment = false;
        }
    }

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->dataFieldByName('URLSegment')->setReadonly(true);
        return $fields;
    }

    public function getSettingsFields() {
        $fields = parent::getSettingsFields();
        $fields->removeByName('AutoUpdateURLSegment');
        return $fields;
    }

    public static function homepage_url_segment() {
        return RootURLController::config()->get('default_homepage_link');
    }

    public static function homepage_link() {
        return Director::absoluteURL(self::homepage_url_segment());
    }
}
